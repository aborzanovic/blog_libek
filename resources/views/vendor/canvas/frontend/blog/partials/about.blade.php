<hr>
<div class="row" style="margin-bottom: 20px;">
    <center>O Slobodnom uglu:</center>
    <p>
        <i>"Slobodni ugao je novi dom slobodarskog duha u Srbiji. Vizija autora Slobodnog ugla jeste stvaranje prostora za argumentovanu raspravu o najrazličitijim
            relevantnim društvenim pitanjima. Kroz kritičko propitivanje izazova naše današnjice, autori različitog profila stvaraju perspektivu slobodnijeg društva.
            To je zajednica odgovornih autora koji zadržavaju i jačaju veru u snagu javno izgovorene reči. Slobodni ugao je mesto rađanja nove političke kulture
            dijaloga koja sa osloncem u iskustvu jednih, teži ohrabrivanju mnogih drugih. Slobodni ugao je autentični ugao promišljanja društva Srbije koje stvaramo
            dok pišemo."</i>
    </p>
</div>

