<style>
    .holder {
        height: 300px;
        position: relative;
    }

    @media (max-width: 992px) {
        .holder {
            height: 330px;
        }
    }

    .img, .txt {
        height: 250px;
        position: absolute;
        top: 0;
        bottom: 0;
        right: 0;
    }

    .img {

        left: 0;
        width: 30%;
    }

    .txt {
        left: 30%;
        padding-left: 10px;
    }

    .h2 {
        margin-top: 0px !important;
    }

    .p-margin {
        margin-bottom: 0px !important;
    }

    h1,h2,h3,h4:hover
    {
        color: #009fd3 !important;
    }

    a:hover, a:focus{
        color: #009fd3 !important;
    }


</style>
<?php $var = 0; ?>
@foreach ($posts as $post)
    <?php $var++; ?>

    <?php if ($var % 2 == 1) echo '<hr>'; ?>
<div class="row">

        <div class="col-md-6 col-md-offset-0 col-xs-10 col-xs-offset-0 holder">

            <a href="{{ $post->url($tag) }}">
                <div class="img"
                     style="background: url('{{$post->page_image}}'); background-position: center; background-repeat: no-repeat; background-size: contain; "></div>
            </a>

            <div class="post-preview txt">

                <h1 class="post-title h2">
                    <a href="{{ $post->url($tag) }}">{{ $post->title }}</a>
                </h1>

                <p class="post-meta">
                    {{ $post->published_at->diffForHumans() }} &#183; {{ $post->readingTime() }} MIN READ
                    <br>
                    @unless( $post->tags->isEmpty())
                        {!! implode(' ', $post->tagLinks()) !!}
                    @endunless
                </p>

                <p class="postSubtitle">
                    {{ str_limit($post->subtitle, config('blog.frontend_trim_width')) }}
                </p>

                <p ><a href="/blog?user={{$post->user->id}}">
                    <img style="margin: 0 15px 0 0; cursor: pointer;" class="img-responsive img-circle author-img "
                         src="https://www.gravatar.com/avatar/{{ md5($post->user->email) }}?d=identicon&s=150"
                         title="{{ $post->user->first_name .  ' ' . $post->user->last_name }}"></a>

                <p class="p-margin" style="font-size: 13px"><a href="{{ $post->url($tag) }}">PROČITAJ JOŠ...</a></p>
                <a href="/blog?user={{$post->user->id}}">
                <h4 id="auth-name" style="float: left;">
                    {{ $post->user->first_name .  ' ' . $post->user->last_name }}</h4>
                </p>
                </a>
            </div>

        </div>

        <?php if ($var % 2 == 0) echo '</div><div class = "row"> '; ?>
    @endforeach
</div>