<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Lozinke moraju sadržati barem 6 karaktera.',
    'reset' => 'Vaša lozinka je resetovana!',
    'sent' => 'Poslali smo vam email sa linkom za resetovanje lozinke!',
    'token' => 'Token za reset lozinke je nevežeći.',
    'user' => "Ne możemo da pronađemo korisnika sa datom email adresom.",

];
